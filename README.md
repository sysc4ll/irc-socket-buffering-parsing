# irc-socket-buffering-parsing

Un bot IRC tout simple.

Le code se compose de la gestion des sockets Windows && UNIX avec l'utilisation de la fonction select() pour ne pas rester bloqué sur les fonctions recv(),
du parseur des lignes IRC (http://abcdrfc.free.fr/rfc-vf/rfc1459.html),
et d'une certaine technique de buffering (buffer circulaire).

```txt
$ make
gcc main.c socket.c parser.c asprintf.c -o simple-bot -Wall
```

https://www.ajulien.fr/irc-socket-buffering-parsing/
